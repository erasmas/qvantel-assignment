(ns fizzbuzz.core
  (:gen-class))

(def fizz "Fizz")

(def buzz "Buzz")

(def fizzbuzz (str fizz buzz))

(defn contains-chars?
  "Checks if a given string contains all given characters."
  [s cs]
  (->> s
       (filter cs)
       (into #{})
       (= cs)))

(defn fizz-buzz
  [n]
  (let [s (str n)
        check-string (partial contains-chars? s)]
    (cond (check-string #{\3 \5}) fizzbuzz
          (check-string #{\3}) fizz
          (check-string #{\5}) buzz
          :else s)))

(defn -main
  [& args]
  (let [from 0
        to 100]
    (doseq [n (range from (+ to 1))]
      (println (format "%d: %s" n (fizz-buzz n))))))
