The solution to this `Fizz Buzz` problem is very straightforward.

I added auxiliary function `contains-chars?` which checks if a given string contains given collection of characters.
After that, it was easy to combine `contains-chars?` with Clojure's `cond` macro to return result
based on the output of `contains-chars?`.

We could have used property based testing using `test.check` but for the sake of simplicity I decided to only use few test cases.    