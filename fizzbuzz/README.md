# fizzbuzz

Prints numbers from 0 to 100, but with a twist.

The twist is: if the number contains 3 (like 3, 13, 30, etc) print "Fizz" instead of the number. Also, if the number contains 5 print "Buzz". If the number contains both 3 and 5, print "FizzBuzz". 

## Usage

Build
```
lein uberjar
```

Execute 
```
$ java -jar target/uberjar/fizzbuzz-*-standalone.jar
```
