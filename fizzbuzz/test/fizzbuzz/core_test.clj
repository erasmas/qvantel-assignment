(ns fizzbuzz.core-test
  (:require [clojure.test :refer :all]
            [fizzbuzz.core :refer :all]
            [clojure.string :as str]))

(deftest fizz-buzz-test
  (testing "Fizz Buzz"

    (testing "returns Fizz for numbers having 3"
      (let [numbers [-3 3 13 23 31 33 30 303]]
        (is (every? #(= fizz %) (map fizz-buzz numbers)))))

    (testing "returns Buzz for numbers having 5"
      (let [numbers [-5 5 15 25 45 50 95 500]]
        (is (every? #(= buzz %) (map fizz-buzz numbers)))))

    (testing "returns FizBuzz for numbers having both 3 and 5"
      (let [numbers [35 53 535 503]]
        (is (every? #(= fizzbuzz %) (map fizz-buzz numbers)))))

    (testing "returns numbers as is, if they don't have 5 or 3"
      (let [numbers [-1 1 2 4 100 99 0]]
        (is (every? #(integer? (Integer/parseInt %)) (map fizz-buzz numbers)))))

    ))
