(ns random-users.views.layout
  (:require [hiccup.page :refer [html5 include-css]]))

(def head
  [:head
   [:title "Users"]
   (include-css "/css/style.css")])

(defn users
  [users]
  (html5
    head
    [:div {:id "content"}
     [:table {:id "users"}
      [:tr [:th "Name"] [:th "E-Mail"]]
      (for [user users]
        [:tr
         [:td (:name user)]
         [:td (:email user)]])]]
    ))
