(ns random-users.routes.home
  (:require [compojure.core :refer :all]
            [random-users.api :refer [get-users]]
            [random-users.views.layout :as layout]))

(defroutes home-routes

  (GET "/" []
    (let [users (get-users )]
      (layout/users users)))

  (GET "/users" [results]
    (let [count (Integer/parseInt results)]
      {:body (get-users count)}))
)
