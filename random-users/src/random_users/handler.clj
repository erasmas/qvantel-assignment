(ns random-users.handler
  (:require [compojure.core :refer [defroutes routes]]
            [ring.middleware.json :refer [wrap-json-response]]
            [hiccup.middleware :refer [wrap-base-url]]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [random-users.routes.home :refer [home-routes]]))

(defroutes app-routes
  (route/resources "/")
  (route/not-found "Not Found"))

(defn wrap-exception [f]
  (fn [request]
    (try (f request)
         (catch Exception e
           (.printStackTrace e)
           {:status 500 :body "We're sorry, something went wrong."}))))

(def app
  (-> (routes home-routes app-routes)
      (handler/site)
      (wrap-base-url)
      (wrap-json-response)
      (wrap-exception)))
