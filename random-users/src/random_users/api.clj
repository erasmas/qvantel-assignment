(ns random-users.api
  (:require [cheshire.core :refer :all]
            [clj-http.client :as client]))

(def url "https://randomuser.me/api")

(def default-users-per-fetch 10)

(defn- format-user
  [user]
  (let [first-name (get-in user [:name :first])
        last-name (get-in user [:name :last])
        name (format "%s %s" first-name last-name)]
    {:name name :email (:email user)}))

(defn- fetch-users
  [n]
  (-> (client/get url {:query-params {:results n}} {:as :json})
      (:body)
      (parse-string true)
      (:results)))

(defn- random-users
  ([]
   (random-users default-users-per-fetch))
  ([n]
   (map format-user (fetch-users n))))

(defn users-seq
  []
  (concat (random-users) (lazy-seq (users-seq))))

(defn get-users
  ([]
   (get-users default-users-per-fetch))
  ([n]
   (take n (users-seq))))