# random-users

## Assignment description

Create a Web application that can show some (for example 10) random users from your solution to problem 2.
The target is to serve a page that works on modern web browsers. 
You can generate the page using any tools you like.
For full points you should use ClojureScript, but if you are not familiar with it you can serve static HTML page too. 


## Prerequisites

You will need [Leiningen][1] 1.7.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein ring server

## License

Copyright © 2017 FIXME
