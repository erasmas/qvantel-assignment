(defproject random-users "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [compojure "1.6.0"]
                 [hiccup "1.0.5"]
                 [clj-http "3.7.0"]
                 [cheshire "5.8.0"]
                 [ring-server "0.4.0"]
                 [ring/ring-json "0.4.0"]
                 [ring/ring-mock "0.3.2"]]
  :plugins [[lein-ring "0.8.12"]]
  
  :ring {:handler random-users.handler/app}

  :main random-users.core

  :profiles
  {:uberjar {:aot :all}
   :production
   {:ring
     {:open-browser? false, :stacktraces? false, :auto-reload? false}
   }
  }
)
