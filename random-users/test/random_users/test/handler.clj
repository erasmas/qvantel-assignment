(ns random-users.test.handler
  (:use clojure.test
        ring.mock.request
        random-users.handler)
  (:require [cheshire.core :refer [parse-string]]))

(deftest test-routes
  (testing "home route"
    (let [response (app (request :get "/"))]
      (is (= (:status response) 200))
      (is (.contains (:body response) "Users"))))

  (testing "not-found route"
    (let [response (app (request :get "/invalid"))]
      (is (= (:status response) 404)))))

(deftest users-handler-test
  (let [size 20
        response (app (request :get (str "/users?results=" size)))
        {body :body status :status headers :headers} response
        users (parse-string body)]
    (is (= status 200))
    (is (= headers {"Content-Type" "application/json; charset=utf-8"}))
    (is (= (count users) size))))
