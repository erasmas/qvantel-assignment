## Running

To start a web server for the application, run:
```
lein ring server
```
    
## Usage

A list of random users, 10 by default, is available at `http://localhost:3000/`.
I'm not that familiar with both CSS and ClojureScript so all you can see on that page is a poor man's HTML.
Sorry about that.

There's also REST API endpoint:
```
curl http://localhost:3000/users\?results\=100
```

Enjoy! :)


